import javax.swing.JOptionPane;
public class Promedios{
	public static void main (String arg[]){
		double valores[]= new double[10];
		double suma=0;
		double calculoPromedio=0;
		int cantMayor=0;
		int cantMenor=0;
		
		for(int elemento=0;elemento<10;elemento++){
			valores[elemento]=Double.parseDouble(JOptionPane.showInputDialog("Digite el valor del número "+(elemento+1)));
		}//fin de llenar arreglo
		
		for(int elemento=0;elemento<valores.length;elemento++){
			suma=suma+valores[elemento];
			calculoPromedio=suma/10;
		}//fin de calcular e imprimir el promedio
		JOptionPane.showMessageDialog(null,"El promedio de los valores del arreglo es "+calculoPromedio);
		
		for(int elemento=0;elemento<valores.length;elemento++){
			if(valores[elemento]>calculoPromedio){
				cantMayor++;
			}//fin del if
		}//fin de valores mayores al promedio
		JOptionPane.showMessageDialog(null,"La cantidad de valores que es mayor al promedio es de "+cantMayor);
		
		for(int elemento=0;elemento<valores.length;elemento++){
			if(valores[elemento]<calculoPromedio){
				cantMenor++;
				JOptionPane.showMessageDialog(null,"El valor del número que es menor al promedio es "+valores[elemento]);
			}//fin del if
		}//fin de valores menores al promedio
		
		for(int elemento=0;elemento<10;elemento++){
          JOptionPane.showMessageDialog(null,"El valor del elemento "+elemento+" es "+valores[elemento]);
	  }//fin del mostrar elementos
  }
}		
