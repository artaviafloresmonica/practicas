import javax.swing.JOptionPane;
public class ArreglosPosicion{
	public static void main (String arg[]){
		int opcion, valor1, valor2, posicionCambio, valorCamb;
		String datoLeido, mayorTexto="",menorTexto="",valorCambTexto="";
		int numeros[]=new int[5];
		opcion=1;
		while(opcion!=8){
		datoLeido=JOptionPane.showInputDialog("------------Arreglos------------"
												+"\n1. Llenar el arreglo."                      
												+"\n2. Mostrar los valores"
												+"\n3. Buscar un valor."
												+"\n4. Mostrar la posicion del valor."
												+"\n5. Mostrar el valor del elemento mayor."
												+"\n6. Mostrar la posicion del valor menor."
												+"\n7. modificar el valor en una posicion"
												+"\n8. Salir");
				opcion=Integer.parseInt(datoLeido);
				
				switch(opcion){
				case 1:
				for(int elemento=0;elemento<5;elemento++){
					if(numeros[elemento]==0){
						numeros[elemento]=(int)(Math.random()*250+1);
					}//fin del if
				}//fin del for
				JOptionPane.showMessageDialog(null,"El arreglo ha sido llenado, de manera aleatoria, correctamente");
				break;
				
				case 2:
				for(int elemento=0;elemento<5;elemento++){
					JOptionPane.showMessageDialog(null,"El valor del elemento "+elemento+" es "+numeros[elemento]);
				}//fin del mostrar elementos
				
				break;
				
				case 3:
				datoLeido=JOptionPane.showInputDialog("ingrese el valor que desea buscar");
				valor1=Integer.parseInt(datoLeido);
				for(int elemento=0;elemento<numeros.length;elemento++){
					if(valor1==numeros[elemento]){
						if(numeros[elemento]!=elemento){
							JOptionPane.showMessageDialog(null,"Elemento " +elemento+ ", el valor ha sido encontrado");
							elemento=numeros.length;
						}
					}
					else{
						if(numeros[elemento]==elemento){
							JOptionPane.showMessageDialog(null,"El valor no ha sido encontrado");
						}
					}
				}
				break;
				
				case 4:
				datoLeido=JOptionPane.showInputDialog("ingrese el valor que desea buscar");
				valor2=Integer.parseInt(datoLeido);
				for(int elemento=0;elemento<numeros.length;elemento++){
					if(valor2==numeros[elemento]){
						if(numeros[elemento]!=elemento){
							JOptionPane.showMessageDialog(null,"el valor digitado "+valor2+" se encuentra en la posición " +elemento);
							elemento=numeros.length;
						}
					}
				}
				JOptionPane.showMessageDialog(null,"\nNota: \nsi anteriormente no le ha salido \nningún mensaje diferente de este, \nsignifica que el número digitado no se \nencuentra en ninguna posición");
				break;
				
				case 5:
				int mayor;
				mayor=numeros[0];
				for(int elemento=0;elemento<numeros.length;elemento++){
					if(mayor<=numeros[elemento]){
						mayor=numeros[elemento];
						mayorTexto="El elemento "+elemento+" con valor de "+numeros[elemento]+" es el mayor";
					}
				}
				JOptionPane.showMessageDialog(null,mayorTexto);
				break;
				
				case 6:
				int menor;
				menor=numeros[0];
				for(int elemento=0;elemento<numeros.length;elemento++){
					if(menor>=numeros[elemento]){
						menor=numeros[elemento];
						menorTexto="El elemento "+elemento+" con valor de "+numeros[elemento]+" es el menor";
					}
				}
				JOptionPane.showMessageDialog(null,menorTexto);
				break;
				
				case 7:
				datoLeido=JOptionPane.showInputDialog("ingrese la posición que desea cambiar");
				posicionCambio=Integer.parseInt(datoLeido);
				datoLeido=JOptionPane.showInputDialog("ingrese el valor que desea modificar");
				valorCamb=Integer.parseInt(datoLeido);
				for(int elemento=0;elemento<numeros.length;elemento++){
					elemento=posicionCambio;
					numeros[elemento]=valorCamb;
					valorCambTexto="El elemento "+elemento+" ha sido cambiado a "+numeros[elemento]+" exitosamente";
				}
				JOptionPane.showMessageDialog(null,valorCambTexto);
				break;
				
				case 8:
				JOptionPane.showMessageDialog(null,"fue un gusto ayudarle");
				break;
				
				default: JOptionPane.showMessageDialog(null,"esta opción no está disponible");
			}//fin del switch
		}//fin del while
	}//fin del main
}//fin de la clase
